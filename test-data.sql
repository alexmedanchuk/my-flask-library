BEGIN TRANSACTION;
CREATE TABLE users (
	id INTEGER NOT NULL, 
	username VARCHAR(64), 
	password_hash VARCHAR(128), 
	PRIMARY KEY (id)
);
INSERT INTO `users` VALUES ('1','test','pbkdf2:sha1:1000$tnDFlGYo$8f54584663ca536eea11e312e597bbdb994975c9');
CREATE TABLE books_autors (
	book_id INTEGER, 
	author_id INTEGER, 
	FOREIGN KEY(book_id) REFERENCES books (id), 
	FOREIGN KEY(author_id) REFERENCES authors (id)
);
INSERT INTO `books_autors` VALUES ('1','1');
INSERT INTO `books_autors` VALUES ('1','2');
INSERT INTO `books_autors` VALUES ('1','3');
INSERT INTO `books_autors` VALUES ('2','4');
INSERT INTO `books_autors` VALUES ('3','5');
INSERT INTO `books_autors` VALUES ('3','6');
INSERT INTO `books_autors` VALUES ('4','7');
INSERT INTO `books_autors` VALUES ('5','8');
INSERT INTO `books_autors` VALUES ('5','9');
INSERT INTO `books_autors` VALUES ('6','12');
INSERT INTO `books_autors` VALUES ('6','10');
INSERT INTO `books_autors` VALUES ('6','11');
INSERT INTO `books_autors` VALUES ('7','13');
INSERT INTO `books_autors` VALUES ('8','14');
INSERT INTO `books_autors` VALUES ('9','16');
INSERT INTO `books_autors` VALUES ('9','15');
INSERT INTO `books_autors` VALUES ('10','17');
INSERT INTO `books_autors` VALUES ('11','18');
INSERT INTO `books_autors` VALUES ('12','18');
CREATE TABLE books (
	id INTEGER NOT NULL, 
	title VARCHAR(64), 
	PRIMARY KEY (id), 
	UNIQUE (title)
);
INSERT INTO `books` VALUES ('1','Web Programming in Python: Techniques for Integrating Linux');
INSERT INTO `books` VALUES ('2','Python Web Programming');
INSERT INTO `books` VALUES ('3','Rapid Web Applications with TurboGears');
INSERT INTO `books` VALUES ('4','CherryPy Essentials: Rapid Python Web Application Development');
INSERT INTO `books` VALUES ('5','The Definitive Guide to Django: Web Development Done Right');
INSERT INTO `books` VALUES ('6','Python Web Development with Django');
INSERT INTO `books` VALUES ('7','The Pylons Book: The Definitive Guide to Pylons');
INSERT INTO `books` VALUES ('8','Pro Django, 2nd Edition');
INSERT INTO `books` VALUES ('9','Two Scoops of Django: Best Practices for Django 1.6');
INSERT INTO `books` VALUES ('10','Learning to Program Using Python');
INSERT INTO `books` VALUES ('11','Programming Python');
INSERT INTO `books` VALUES ('12','Learning Python');
CREATE TABLE authors (
	id INTEGER NOT NULL, 
	name VARCHAR(64), 
	PRIMARY KEY (id), 
	UNIQUE (name)
);
INSERT INTO `authors` VALUES ('1','George K. Thiruvathukal');
INSERT INTO `authors` VALUES ('2','Thomas W. Christopher');
INSERT INTO `authors` VALUES ('3','John P. Shafaee');
INSERT INTO `authors` VALUES ('4','Steve Holden');
INSERT INTO `authors` VALUES ('5','Mark Ramm');
INSERT INTO `authors` VALUES ('6','Kevin Dangoor');
INSERT INTO `authors` VALUES ('7','Sylvain Hellegouarch');
INSERT INTO `authors` VALUES ('8','Adrian Holovaty');
INSERT INTO `authors` VALUES ('9','Jacob Kaplan-Moss');
INSERT INTO `authors` VALUES ('10','Jeff Forcier');
INSERT INTO `authors` VALUES ('11','Paul Bissex');
INSERT INTO `authors` VALUES ('12','Wesley Chun');
INSERT INTO `authors` VALUES ('13','James Gardner');
INSERT INTO `authors` VALUES ('14','Marty Alchin');
INSERT INTO `authors` VALUES ('15','Daniel Greenfeld');
INSERT INTO `authors` VALUES ('16','Audrey Roy');
INSERT INTO `authors` VALUES ('17','Cody Jackson');
INSERT INTO `authors` VALUES ('18','Mark Lutz');
;
;
CREATE UNIQUE INDEX ix_users_username ON users (username);
COMMIT;
