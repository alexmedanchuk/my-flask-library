from flask import redirect, url_for, render_template, flash
from flask.ext.login import login_user, logout_user, login_required, \
							current_user

from libr import db
from . import auth
from .models import User
from .forms import LoginForm, RegisterForm


@auth.route('/login', methods=['POST', 'GET'])
def login():
	"""Logs in user."""
	if current_user.is_authenticated():
		return redirect(url_for('main.books'))
	form = LoginForm()
	if form.validate_on_submit():
		username = form.username.data
		user = User.query.filter_by(username=username).first()
		if user and user.check_password(form.password.data):
			login_user(user)
			flash("You're successfully logged in to Library")
			return redirect(url_for('main.books'))
		flash("Invalid password or username!")
	return render_template('auth/login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
	"""Logs out user."""
	logout_user()
	flash("You're successfully logged out from Library!")
	return redirect(url_for('main.books'))


@auth.route('/register', methods=['POST', 'GET'])
def register():
	if current_user.is_authenticated():
		return redirect(url_for('main.books'))
	form = RegisterForm()
	if form.validate_on_submit():
		u = User(username=form.username.data,
				password=form.password.data)
		db.session.add(u)
		db.session.commit()
		flash("You're successfully registered in Library!")
		return redirect(url_for('auth.login'))
	return render_template('auth/register.html', form=form)