from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, SubmitField, ValidationError
from wtforms.validators import Required, Length, Regexp, EqualTo

from .models import User


class LoginForm(Form):
	"""Users authentication form."""
	username = StringField('Username', validators=[Required(), Length(1, 64)])
	password = PasswordField('Password', validators=[Required()])
	submit = SubmitField('Log In')


class RegisterForm(Form):
	"""User registration form."""
	username = StringField('Username', validators=[
	Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
		'Usernames must have only letters, '
		'numbers, dots or underscores')])
	password = PasswordField('Password', validators=[
		Required(), EqualTo('password2', message='Passwords must match.')])
	password2 = PasswordField('Confirm password', validators=[Required()])
	submit = SubmitField('Register')
	
	def validate_username(self, field):
		"""Validates for duplicated usernames."""
		if User.query.filter_by(username=field.data).first():
			raise ValidationError('Username already in use.')