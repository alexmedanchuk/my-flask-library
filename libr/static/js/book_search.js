$(document).ready(function() {

	$("#search-button").click(function() {
		$("#back-div").html('<a href="' + window.location.pathname + '" class="btn btn-success">Back to books</a>');
    	var match = $("#search-input").val();
	    $.ajax({
	    url : "/search/book",
	    type : "GET",
	    dataType: "html",
	    data : {
			'match' : match,
	    },
	    success : searchSuccess
	    });
	});
function searchSuccess(data, textStatus, jqXHR) {
    $(".result-table").html(data);
};

});