from flask import render_template
from . import main


@main.app_errorhandler(404)
def page_not_found(e):
	return render_template('base.html', title='404 Page Not Found'), 404


@main.app_errorhandler(405)
def internal_server_error(e):
	return render_template('base.html', title='405 Method Not Allowed'), 405


@main.app_errorhandler(500)
def internal_server_error(e):
	return render_template('base.html', title='500 Internal Server Error'), 500
