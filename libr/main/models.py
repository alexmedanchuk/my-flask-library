from libr import db


# Association table for books and authors
books_autors = db.Table('books_autors',
	db.Column('book_id', db.Integer, db.ForeignKey('books.id')),
	db.Column('author_id', db.Integer, db.ForeignKey('authors.id'))
	)


class Author(db.Model):
	"""Author entry model."""

	__tablename__='authors'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64), unique=True)
	books = db.relationship('Book',
							secondary=books_autors,
							backref=db.backref('authors', lazy='joined'),
							lazy='joined')


class Book(db.Model):
	"""Book entry model."""

	__tablename__='books'
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(64), unique=True)