from flask import abort, render_template, redirect, url_for, request, flash
from flask.ext.login import login_required

from libr import db
from . import main
from .models import Author, Book
from .forms import BookEditForm, AuthorEditForm, DeleteForm


@main.route('/authors')
def authors():
	"""Return page with list of all authors."""
	authors = Author.query.all()
	del_form = DeleteForm()
	return render_template('main/authors.html', authors=authors, form=del_form)


@main.route('/')
def books():
	"""Return page with list of all books."""
	books = Book.query.all()
	del_form = DeleteForm()
	return render_template('main/books.html', books=books, form=del_form)


@main.route('/author/detail/<int:id>')
def author_detail(id):
	"""Single author entry view.

	Return page with detailed information about author 
	with Author ID equal to parameter 'id' or 404 error if such
	Author ID does not exist.
	"""
	author = Author.query.get_or_404(id)
	form = DeleteForm()
	return render_template('main/author_detail.html', author=author, form=form)


@main.route('/book/detail/<int:id>')
def book_detail(id):
	"""Single book entry view.

	Return page with detailed information about book 
	with Book ID equal to parameter 'id' or 404 error if such
	Book ID does not exist.
	"""
	book = Book.query.get_or_404(id)
	form = DeleteForm()
	return render_template('main/book_detail.html', book=book, form=form)


@main.route('/book/add', methods=['POST', 'GET'])
@login_required
def add_book():
	"""Adds new book entry to the database. 
	
	Requires user authentication.
	"""
	form = BookEditForm()
	if form.validate_on_submit():
		book = Book(title=form.title.data)
		if form.authors.data:
			book.authors.extend(form.authors.data)
		db.session.add(book)
		db.session.commit()
		flash("Book entry successfully added.")
		if request.args.get('next'):
			return redirect(request.args.get('next'))
		return redirect(url_for('main.books'))
	return render_template('main/add_book.html', form=form)


@main.route('/author/add', methods=['POST', 'GET'])
@login_required
def add_author():
	"""Adds new author entry to the database. 
	
	Requires user authentication.
	"""
	form = AuthorEditForm()
	if form.validate_on_submit():
		author = Author(name=form.name.data)
		if form.books.data:
			author.books.extend(form.books.data)
		db.session.add(author)
		db.session.commit()
		flash("Author entry was successfully added.")
		if request.args.get('next'):
			return redirect(request.args.get('next'))
		return redirect(url_for('main.authors'))
	return render_template('main/add_author.html', form=form)


@main.route('/book/<int:id>', methods=['POST', 'GET'])
@login_required
def edit_book(id):
	"""Edits book entry. 
	
	Requires user authentication. Returns 404 error if Book ID does
	not exist.
	"""
	book = Book.query.get_or_404(id)
	form = BookEditForm(obj=book)
	if form.validate_on_submit():
		book.title = form.title.data
		book.authors = []
		if form.authors.data:
			book.authors.extend(form.authors.data)
		db.session.commit()
		flash("Book entry was successfully edited.")
		return redirect(url_for('main.books'))
	return render_template('main/edit_book.html', form=form, book=book)


@main.route('/author/<int:id>', methods=['POST', 'GET'])
@login_required
def edit_author(id):
	"""Edits author entry. 
	
	Requires user authentication. Returns 404 error if Author ID does
	not exist.
	"""
	author = Author.query.get_or_404(id)
	form = AuthorEditForm(obj=author)
	if form.validate_on_submit():
		author.name = form.name.data
		author.books = []
		if form.books.data:
			author.books.extend(form.books.data)
		db.session.commit()
		flash("Author entry was successfully edited.")
		return redirect(url_for('main.authors'))
	return render_template('main/edit_author.html', form=form, author=author)


@main.route('/delete/author' , methods=['POST'])
@login_required
def delete_author():
	"""Deletes author entry. 
	
	Requires user authentication and only POST requests. Returns 404 error 
	if Author ID does not exist.
	"""
	form = DeleteForm()
	id = form.id.data
	if id:
		author = Author.query.get_or_404(id)
		db.session.delete(author)
		db.session.commit()
		flash("Author entry was successfully deleted.")
		return redirect(url_for('main.authors'))
	abort(404)


@main.route('/delete/book' , methods=['POST'])
@login_required
def delete_book():
	"""Deletes book entry. 
	
	Requires user authentication and only POST requests. Returns 404 error 
	if Book ID does not exist.
	"""
	form = DeleteForm()
	id = form.id.data
	if id:
		book = Book.query.get_or_404(id)
		db.session.delete(book)
		db.session.commit()
		flash("Book entry was successfully deleted.")
		return redirect(url_for('main.books'))
	abort(404)


@main.route('/search/book')
def search_book():
	"""Book entry search.

	Takes 'match' argument from request and return list of books that matched.
	Checks match for book title and authors names fields.
	"""
	match = request.args.get('match')
	match = '%{}%'.format(match)
	books = Book.query.filter(Book.authors.any(Author.name.like(match)) + Book.title.like(match))
	return render_template('main/book-search-results.html', books=books)


@main.route('/search/author')
def search_author():
	"""Author entry search.

	Takes 'match' argument from request and return list of authors that matched.
	Checks match for author name fiels.
	"""
	match = request.args.get('match')
	match = '%{}%'.format(match)
	authors = Author.query.filter(Author.name.like(match))
	return render_template('main/author-search-results.html', authors=authors)