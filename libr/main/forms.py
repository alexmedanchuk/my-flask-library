from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, HiddenField, ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField
from wtforms.validators import Required, Length, Regexp

from .models import Book, Author


def all_books():
	"""Returns books query."""
	return Book.query


def all_authors():
	"""Returns authors query."""
	return Author.query


class BookEditForm(Form):
	"""Form for adding and editing book entries."""
	title = StringField('Book title', validators=[Required(), Length(1, 64), 
			Regexp('^[^<>]+$',
			0, 'Book title must not have "<" or ">" charachters')])
	authors = QuerySelectMultipleField('Add Author(s)', 
										query_factory=all_authors, 
										get_label='name')
	submit = SubmitField('Save')

	def validate_title(self, field):
		"""Validates if book title starts with a capital letter."""
		if not field.data[0].isalpha() or \
				field.data[0].lower() == field.data[0]:
			raise ValidationError('Title must begin with capital letter.')


class AuthorEditForm(Form):
	"""Form for adding and editing author entries."""
	name = StringField('Author', validators=[Required(), Length(2, 64), 
			Regexp('^[^<>]+$', 
			0, 'Author name must not have "<" or ">" charachters')])
	books = QuerySelectMultipleField('Add Book(s)', 
									query_factory=all_books, 
									get_label='title')
	submit = SubmitField('Save')

	def validate_name(self, field):
		"""Validates if author name starts with a capital letter."""
		if not field.data[0].isalpha() or \
				field.data[0].lower() == field.data[0]:
			raise ValidationError('Author name must begin with capital letter.')


class DeleteForm(Form):
	"""Form for deleting entries."""
	id = HiddenField()
		
