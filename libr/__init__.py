import os

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.bootstrap import Bootstrap
from flask.ext.login import LoginManager


BASEDIR = os.path.abspath(os.path.dirname(__file__))


libr = Flask(__name__)

if os.environ.get('DATABASE_URL') is None:
	SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'libdb')
else:
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

libr.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
libr.config['SQLALCHEMY_ECHO'] = True
libr.config['SECRET_KEY'] = 'dsklajdlkajldjalkjdlakjdlkajldkajcnsjna'


db = SQLAlchemy(libr)
bootstrap = Bootstrap(libr)
login_manager = LoginManager(libr)
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'


from .main import main as main_bp
from .auth import auth as auth_bp


libr.register_blueprint(main_bp)
libr.register_blueprint(auth_bp)

if __name__ == "__main__":
	libr.run()